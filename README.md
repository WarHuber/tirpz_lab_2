## Національний технічний університет України<br>“Київський політехнічний інститут ім. Ігоря Сікорського”

## Факультет прикладної математики<br>Кафедра системного програмування і спеціалізованих комп’ютерних систем

# Лабораторна робота №1<br>"Базова робота з git"

## КВ-13 Луценко Богдан

---

## Хід виконання роботи

### 1. Зклонувати репозиторій для ЛР2 використавши локальний репозиторій від ЛР1 в якості "віддаленого"

Клонуємо

```git
student@virt-linux:~/tirpz/lab2$ git clone file:///home/student/tirpz/lab1/pandoc
Cloning into 'pandoc'...
remote: Enumerating objects: 118611, done.
remote: Counting objects: 100% (118611/118611), done.
remote: Compressing objects: 100% (35681/35681), done.
remote: Total 118611 (delta 75393), reused 118611 (delta 75393)s
Receiving objects: 100% (118611/118611), 57.79 MiB | 25.71 MiB/s, done.
Resolving deltas: 100% (75393/75393), done.
```

Перевіримо, що в копії репозиторію ЛР2 у нас присутні гілки, які ми створювали для ЛР1:

```git
git branch -a
* main
  remotes/origin/HEAD -> origin/main
  remotes/origin/main
```

Перевіримо, куди "дивиться" наш поточний ремоут origin:

```git
student@virt-linux:~/tirpz/lab2/pandoc$ git remote -v
origin	file:///home/student/tirpz/lab1/pandoc (fetch)
origin	file:///home/student/tirpz/lab1/pandoc (push)
```

### 2. Робота з ремоутами

#### 1. Додати новий ремоут використавши URI на інтернет-джерело вашого репозиторію

Додамо ще один ремоут до нашої локальної копії:

```git
student@virt-linux:~/tirpz/lab2/pandoc$ git remote add upstream https://github.com/jgm/pandoc.git
```

І перевіримо список ремоутів:

```git
student@virt-linux:~/tirpz/lab2/pandoc$ git remote -v
origin	file:///home/student/tirpz/lab1/pandoc (fetch)
origin	file:///home/student/tirpz/lab1/pandoc (push)
upstream	https://github.com/jgm/pandoc.git (fetch)
upstream	https://github.com/jgm/pandoc.git (push)
```

#### 2. Показати список віддалених гілок, так щоби було видно гілки з різних ремоутів

Отримаємо список змін з нового ремоута

```git
git fetch upstream
...
student@virt-linux:~/tirpz/lab2/pandoc$ git branch -a
* main
  remotes/origin/HEAD -> origin/main
  remotes/origin/main
  remotes/upstream/aeson-pretty
  remotes/upstream/babel
  remotes/upstream/bespoke-prelude
  remotes/upstream/chunkedhtml
  remotes/upstream/commonmark-github
  remotes/upstream/commonmark-hs
  remotes/upstream/commonmark-hs-new
  remotes/upstream/dark-mode
  remotes/upstream/endnote
  remotes/upstream/figures
  remotes/upstream/free
  remotes/upstream/ghc925-3.1.5
  remotes/upstream/ghc925-text2-3.1.5
  remotes/upstream/ghc942-3.1.5
  remotes/upstream/ghc944-3.1.5
  remotes/upstream/images2
  remotes/upstream/initialize-data-files
  remotes/upstream/issue-6072
  remotes/upstream/issue1234
  remotes/upstream/issue221
```

#### 3. Створити нову гілку lab2-branch, додати в нього кілька комітів.

```git
student@virt-linux:~/tirpz/lab2/pandoc$ git checkout -b lab2-branch
Switched to a new branch 'lab2-branch'
...
# додали кілька комітів в нову гілку
...
```

#### 4. Пушнути гілку lab2-branch на ремоут, створений з ЛР1 без зв'язування локальної гілки з віддаленою

```git
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git push
fatal: The current branch lab2-branch has no upstream branch.
To push the current branch and set the remote as upstream, use

    git push --set-upstream origin lab2-branch
```

Але при спробі залити ці зміни на віддалений сервер гіт видав наступну помилку. Чому? Бо він не хоче брати на себе відповідальність обирати, на яку саме віддалену копію репозиторію ви хочете покласти нову гілку

#### 5. Додати до гілки ще коміт.

```git
student@virt-linux:~/tirpz/lab2/pandoc/windows$ touch nnn.txt
student@virt-linux:~/tirpz/lab2/pandoc/windows$ nano nnn.txt
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git add nnn.txt
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git commit -m "add nnn.txt"
[lab2-branch b2c12aa6e] add nnn.txt
 1 file changed, 1 insertion(+)
 create mode 100644 windows/nnn.txt
```

#### 6. Пушнути зміни гілки на ремоут, створений з ЛР1, цього разу зв'язавши гілки

```git
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git push -u origin lab2-branch
Enumerating objects: 14, done.
Counting objects: 100% (14/14), done.
Delta compression using up to 2 threads
Compressing objects: 100% (10/10), done.
Writing objects: 100% (12/12), 1023 bytes | 341.00 KiB/s, done.
Total 12 (delta 6), reused 0 (delta 0)
To file:///home/student/tirpz/lab1/pandoc
 * [new branch]          lab2-branch -> lab2-branch
Branch 'lab2-branch' set up to track remote branch 'lab2-branch' from 'origin'
```

#### 7. Додати до гілки ще коміт

```git
student@virt-linux:~/tirpz/lab2/pandoc/windows$ touch nnnn.txt
student@virt-linux:~/tirpz/lab2/pandoc/windows$ nano nnnn.txt
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git add nnnn.txt
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git commit -m "add nnnn.txt"
[lab2-branch cd77351fa] add nnnn.txt
 1 file changed, 1 insertion(+)
 create mode 100644 windows/nnnn.txt
```

#### 8. Переконатися в тому, що після зв'язування гілок тепер можна пушити просто через git push.

```git
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git push
Enumerating objects: 6, done.
Counting objects: 100% (6/6), done.
Delta compression using up to 2 threads
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 375 bytes | 375.00 KiB/s, done.
Total 4 (delta 2), reused 0 (delta 0)
To file:///home/student/tirpz/lab1/pandoc
   b2c12aa6e..cd77351fa  lab2-branch -> lab2-branch
```

#### 9. Перевірити в репозиторії ЛР1, що після пушу тут з'явилася нова локальна гілка (яку ми власне пушнули).

```git
student@virt-linux:~/tirpz/lab1/pandoc/windows$ git log --graph --oneline --all
* cd77351fa (lab2-branch) add nnnn.txt
* b2c12aa6e add nnn.txt
* 9c3444c6f add new file 2
* 0a0215b7e new file
```

### 3. Змерджити гілку, що була створена при виконанні ЛР1, в поточну гілку lab2-branch

Перевіримо поточний стан гілок

```git
student@virt-linux:~/tirpz/lab2/pandoc$ git branch -a
* lab2-branch
  my-branch
  remotes/origin/HEAD -> origin/my-branch
  remotes/origin/lab2-branch
  remotes/origin/main
  remotes/origin/my-branch
  ...
```

Бачимо, що локальної копії гілки ЛР1 у нас наразі немає.

```git
student@virt-linux:~/tirpz/lab2/pandoc$ git merge origin/my-branch
Merge made by the 'recursive' strategy.
 windows/t.txt | 1 +
 1 file changed, 1 insertion(+)
 create mode 100644 windows/t.txt
student@virt-linux:~/tirpz/lab2/pandoc$ git log --pretty=oneline --graph
*   1ef6eedb9c7ac888c00588c167ca939c1031711a (HEAD -> lab2-branch) Merge remote-tracking branch 'origin/my-branch' into lab2-branch
|\  
| * 5d167fdc81cc3056d286fa8b67043aadfe9710b0 (origin/my-branch, origin/HEAD, my-branch) add new file t
* | cd77351fa3e18599e8fb80196e35a5c961503240 (origin/lab2-branch) add nnnn.txt
* | b2c12aa6e5ffb977a5e16136554d9cdbaf6d1276 add nnn.txt
* | 9c3444c6fd2b3758a617c57c442c0a84acc47ebc add new file 2
* | 0a0215b7ea285d05fe6ce17221bb196ba653f068 new file
|/ 
```

### 4. Перенесення комітів.

#### 1. Створити ще одну гілку від master-a lab2-branch-2, додати в неї три коміти.

```git
student@virt-linux:~/tirpz/lab2/pandoc$ git checkout -b lab2-branch-2
Switched to a new branch 'lab2-branch-2'
student@virt-linux:~/tirpz/lab2/pandoc/windows$ touch first.txt
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git status
On branch lab2-branch-2
Untracked files:
  (use "git add <file>..." to include in what will be committed)
	first.txt

nothing added to commit but untracked files present (use "git add" to track)
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git add .
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git commit -m "add first file"
[lab2-branch-2 cba5c49db] add first file
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 windows/first.txt
student@virt-linux:~/tirpz/lab2/pandoc/windows$ touch middle.txt
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git add .
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git commit -m "add middle file"
[lab2-branch-2 4413ac0f7] add middle file
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 windows/middle.txt
student@virt-linux:~/tirpz/lab2/pandoc/windows$ touch last.txt
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git add .
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git commit -m "add last file"
[lab2-branch-2 eff590d75] add last file
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 windows/last.txt
```

#### 2. Перенести з гілки lab2-branch-2 середній з трьох нових комітів в гілку lab2-branch.

```git
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git log
commit eff590d75c77f04b2a58bb290cce94596458b867 (HEAD -> lab2-branch-2)
Author: Bohdan Lutsenko <gaffchd@gmail.com>
Date:   Thu Dec 28 13:46:44 2023 +0200

    add last file

commit 4413ac0f771508e09e2a9a82900b240eb2bcb52c
Author: Bohdan Lutsenko <gaffchd@gmail.com>
Date:   Thu Dec 28 13:46:20 2023 +0200

    add middle file

commit cba5c49db482d444fa6d1cbbb2b1fbf7d32f3252
Author: Bohdan Lutsenko <gaffchd@gmail.com>
Date:   Thu Dec 28 13:45:35 2023 +0200

    add first file
```

Хещ потрібного нам коміту = 4413ac0f771508e09e2a9a82900b240eb2bcb52c

```git
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git checkout lab2-branch
Switched to branch 'lab2-branch'
Your branch is ahead of 'origin/lab2-branch' by 2 commits.
  (use "git push" to publish your local commits)
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git cherry-pick 4413ac0f771508e09e2a9a82900b240eb2bcb52c
[lab2-branch 5c2aec240] add middle file
 Date: Thu Dec 28 13:46:20 2023 +0200
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 windows/middle.txt
student@virt-linux:~/tirpz/lab2/pandoc/windows$  git log --pretty=oneline --graph -n 10 --branches
* 5c2aec2402111dad33aa5946e1fd25b98c0bdf6d (HEAD -> lab2-branch) add middle file
| * eff590d75c77f04b2a58bb290cce94596458b867 (lab2-branch-2) add last file
| * 4413ac0f771508e09e2a9a82900b240eb2bcb52c add middle file
| * cba5c49db482d444fa6d1cbbb2b1fbf7d32f3252 add first file
|/  
*   1ef6eedb9c7ac888c00588c167ca939c1031711a Merge remote-tracking branch 'origin/my-branch' into lab2-branch
|\  
| * 5d167fdc81cc3056d286fa8b67043aadfe9710b0 (origin/my-branch, origin/HEAD, my-branch) add new file t
* | cd77351fa3e18599e8fb80196e35a5c961503240 (origin/lab2-branch) add nnnn.txt
* | b2c12aa6e5ffb977a5e16136554d9cdbaf6d1276 add nnn.txt
* | 9c3444c6fd2b3758a617c57c442c0a84acc47ebc add new file 2
* | 0a0215b7ea285d05fe6ce17221bb196ba653f068 new file
|/  
```

### 5. Визначити останнього спільного предка між двома будь-якими гілками.

```git
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git merge-base origin/lab2-branch origin/my-branch
d3d0406515cb6aae6f43e60dfa37014bdd91e79c
```

### 6. Робота з ничкою

#### 1. Зробити трохи unstaged змін.

```git
student@virt-linux:~/tirpz/lab2/pandoc/windows$ nano n.txt
student@virt-linux:~/tirpz/lab2/pandoc/windows$ nano nn.txt
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git status
On branch lab2-branch
Your branch is ahead of 'origin/lab2-branch' by 3 commits.
  (use "git push" to publish your local commits)

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   n.txt
	modified:   nn.txt

no changes added to commit (use "git add" and/or "git commit -a")
```

#### 2. Зберегти до нички.

```git
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git stash
Saved working directory and index state WIP on lab2-branch: 5c2aec240 add middle file
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git status
On branch lab2-branch
Your branch is ahead of 'origin/lab2-branch' by 3 commits.
  (use "git push" to publish your local commits)

nothing to commit, working tree clean
```

#### 3. Зробити ще трохи unstaged змін

```git
student@virt-linux:~/tirpz/lab2/pandoc/windows$ nano nnn.txt
```

#### 4. Зберегти до нички.

```git
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git stash
Saved working directory and index state WIP on lab2-branch: 5c2aec240 add middle file
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git stash list
stash@{0}: WIP on lab2-branch: 5c2aec240 add middle file
stash@{1}: WIP on lab2-branch: 5c2aec240 add middle file
```

#### 5. Дістати з нички перші збережені зміни, ті що збереглися на кроці (6.2)

```git
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git stash
Saved working directory and index state WIP on lab2-branch: 5c2aec240 add middle file
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git status
On branch lab2-branch
Your branch is ahead of 'origin/lab2-branch' by 3 commits.
  (use "git push" to publish your local commits)

nothing to commit, working tree clean
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git stash list
stash@{0}: WIP on lab2-branch: 5c2aec240 add middle file
stash@{1}: WIP on lab2-branch: 5c2aec240 add middle file
stash@{2}: WIP on lab2-branch: 5c2aec240 add middle file
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git stash apply stash@{2}
On branch lab2-branch
Your branch is ahead of 'origin/lab2-branch' by 3 commits.
  (use "git push" to publish your local commits)

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   n.txt
	modified:   nn.txt

no changes added to commit (use "git add" and/or "git commit -a")
```

### 7. Робота з файлом .gitignore.

#### 1. Створити кілька файлів з якимось унікальним розширенням

```git
student@virt-linux:~/tirpz/lab2/pandoc/windows$ touch 1.kvfpm
student@virt-linux:~/tirpz/lab2/pandoc/windows$ touch 2.kvfpm
student@virt-linux:~/tirpz/lab2/pandoc/windows$ touch nore.txt
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git status
On branch lab2-branch
Your branch is ahead of 'origin/lab2-branch' by 3 commits.
  (use "git push" to publish your local commits)

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	1.kvfpm
	2.kvfpm
	nore.txt

nothing added to commit but untracked files present (use "git add" to track)
```

Додамо шаблон для ігнорування

```
*.kvfpm
```

```git
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git status
On branch lab2-branch
Your branch is ahead of 'origin/lab2-branch' by 3 commits.
  (use "git push" to publish your local commits)

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   ../.gitignore

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	nore.txt

no changes added to commit (use "git add" and/or "git commit -a")
```

#### 4. Перевірити статус включно з ігнором.

```git
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git status --ignored
On branch lab2-branch
Your branch is ahead of 'origin/lab2-branch' by 3 commits.
  (use "git push" to publish your local commits)

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   ../.gitignore

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	nore.txt

Ignored files:
  (use "git add -f <file>..." to include in what will be committed)
	../first.txt
	1.kvfpm
	2.kvfpm

no changes added to commit (use "git add" and/or "git commit -a")
```

#### 5. Почистити всі untracked файли з репозиторію, включно з ігнорованими.

```git
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git clean -fdx
Removing 1.kvfpm
Removing 2.kvfpm
Removing nore.txt
```

### 8. Робота з reflog.

```git
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git checkout main
M	.gitignore
Branch 'main' set up to track remote branch 'main' from 'origin'.
Switched to a new branch 'main'
student@virt-linux:~/tirpz/lab2/pandoc/windows$  git branch -D lab2-branch
Deleted branch lab2-branch (was 5c2aec240).
student@virt-linux:~/tirpz/lab2/pandoc/windows$  git push -d origin lab2-branch
To file:////home/student/tirpz/lab1/pandoc
 - [deleted]             lab2-branch
```

#### 1. Переглянути лог станів гілок.

```git
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git reflog
d3d040651 (HEAD -> main, origin/main) HEAD@{0}: checkout: moving from lab2-branch to main
5c2aec240 HEAD@{1}: reset: moving to HEAD
5c2aec240 HEAD@{2}: reset: moving to HEAD
5c2aec240 HEAD@{3}: reset: moving to HEAD
5c2aec240 HEAD@{4}: reset: moving to HEAD
5c2aec240 HEAD@{5}: cherry-pick: add middle file
1ef6eedb9 HEAD@{6}: checkout: moving from lab2-branch-2 to lab2-branch
eff590d75 (lab2-branch-2) HEAD@{7}: commit: add last file
4413ac0f7 HEAD@{8}: commit: add middle file
cba5c49db HEAD@{9}: commit: add first file
1ef6eedb9 HEAD@{10}: checkout: moving from lab2-branch to lab2-branch-2
1ef6eedb9 HEAD@{11}: merge origin/my-branch: Merge made by the 'recursive' strategy.
cd77351fa HEAD@{12}: checkout: moving from my-branch to lab2-branch
5d167fdc8 (origin/my-branch, origin/HEAD, my-branch) HEAD@{13}: clone: from file:////home/student/tirpz/lab1/pando
```

```git
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git branch lab2-resurrected 5c2aec240
student@virt-linux:~/tirpz/lab2/pandoc/windows$ git checkout lab2-resurrected 
M	.gitignore
Switched to branch 'lab2-resurrected'
```